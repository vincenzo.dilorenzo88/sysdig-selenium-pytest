from selenium.webdriver.common.by import By


class LoginPageLocators:
    
    USERNAME = (By.XPATH, "//input[@placeholder='Enter your email address']")
    PASSWORD = (By.XPATH, "//input[@placeholder='Enter your password']")
    LOGIN_BUTTON = (By.CSS_SELECTOR, "button[class='ember-view simple-btn simple-btn--login']")
    LOGIN_ERROR = (By.CSS_SELECTOR, "p[class='login__error-display']")
    FORGOT_PASS_LINK = (By.LINK_TEXT, 'Forgot your password?')
    SIGN_UP = (By.LINK_TEXT, 'Sign up for a free trial!')
    DROPDOWN_TAB = (By.CSS_SELECTOR, "div[class='reactsel__indicators css-1wy0on6']")
    DROPDOWN_EU = (By.CSS_SELECTOR, "div[class='reactsel__option reactsel__option--is-focused css-7etfs7-option']")
    DROPDOWN_US_EST = (By.CSS_SELECTOR, "div[class='reactsel__option reactsel__option--is-selected css-1ou9e0u-option']")
    DROPDOWN_US_WEST = (By.XPATH, "//*[contains(text(),'US West')]")
    DROPDOWN_AP = (By.XPATH, "//*[@title='AP - app.au1.sysdig.com']")
    GOOGLE_LINK = (By.CSS_SELECTOR, "img[class='block-login__third-party-button--google-logo']")
    SAML_LINK = (By.CSS_SELECTOR, "img[class='block-login__third-party-button--saml-logo']")
    OPENID_LINK = (By.CSS_SELECTOR, "img[class='block-login__third-party-button--openid-logo']")


