import pytest
from selenium import webdriver
# from selenium.webdriver.firefox.options import Options
# from selenium.webdriver.firefox.service import Service
# from webdriver_manager.firefox import GeckoDriverManager




@pytest.fixture(scope="module")
def SetUp():
    options = webdriver.FirefoxOptions()
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    options.headless = True
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920x1080")
    driver = webdriver.Remote(command_executor='http://localhost:4444/wd/hub', options=options)
    yield driver
    driver.close()


@pytest.fixture
def logging_url():
    url = "http://app.sysdigcloud.com/"
    return url


# @pytest.fixture
# def firefox_options(firefox_options):
#     firefox_options.add_argument("-headless")
#     return firefox_options

# @pytest.fixture
# def chrome_options(chrome_options):
#     chrome_options.add_argument("-headless")
#     return chrome_options

# @pytest.fixture
# def selenium(selenium):
#     selenium.set_window_size(1920, 1080)
#     selenium.maximize_window()
#     return selenium