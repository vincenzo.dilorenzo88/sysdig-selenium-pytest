# SysDig assessment

This is a python project that will allow you to test all the web elements of the http://app.sysdigcloud.com/ login web page using Selenium WebDriver and pytest as the tesing frameworks. 
This project has been tested with both Python 3.7.9 and Python 3.8.4 on Windows and on Linux OS. For the sake of the test, only Firefox browser has been used to validate the defined tests. The extra packages installed (listed in the requirements.txt file) are selenium, pytest and webdriver-manager. In the following steps, python 3.7.9 has been taken as an example to setup the testing environment up.

## Getting started

## Setup

### Windows

Install [Python 3.7.9](https://www.python.org/downloads/release/python-379/) and make sure that during installation it is added to PATH.
Confirm that `py` designates Python 3.7.9:
```Powershell
py --version
Python 3.7.9
```
Upgrade pip to the latest version available, currently 
```Powershell
python -m pip install --upgrade pip
py -m pip --version
pip 22.0.4 from c:\home_directory\work_directory\sysdig-assestment\venv\lib\site-packages\pip (python 3.7)
```
Create a virtual environment in the repository root:
```Powershell
python -m pip install --user virtualenv
py -m venv venv
venv\Scripts\activate
```
Finally install all the required packages from the requirements.txt file 
```Powershell
pip install -r requirements.txt
```
### Linux
Install [Python 3.7.9](https://www.python.org/downloads/release/python-379/) and make sure that during installation it is added to PATH.
Confirm that `py` designates Python 3.7.9:
```Powershell
py --version
Python 3.7.9
```
Upgrade pip to the latest version available, currently 
```Powershell
python3 -m pip install --user --upgrade pip
python3 -m pip --version
pip 22.0.4 from $HOME/.local/lib/python3.7/site-packages/pip (python 3.7)
```
Create a virtual environment in the repository root and activate it:
```Powershell
python3 -m pip install --user virtualenv
python3 -m venv venv
source env/bin/activate
```
Finally install all the required packages from the requirements.txt file 
```Powershell
pip install -r requirements.txt
```

### Docker

Install Docker and Docker-compose (this installation has been done on Fedora 35)

```Powershell
sudo dnf install dnf-plugins-core -y
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker
```
Verify the that docker has been correctly installed checking the version and downloading the "hello-world" image

```Powershell
sudo docker version
sudo docker run hello-world
sudo docker -help
sudo docker images
```
Now install docker-compose
```Powershell
sudo dnf -y install wget
curl -s https://api.github.com/repos/docker/compose/releases/latest  | grep browser_download_url   | grep docker-compose-linux-x86_64  | cut -d '"' -f 4   | wget -qi -
chmod +x docker-compose-linux-x86_64
sudo mv docker-compose-linux-x86_64 /usr/local/bin/docker-compose
 sudo curl -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
source /etc/bash_completion.d/docker-compose 
```
Verify that docker-compose has been correctly installed issuing the version command
```Powershell
sudo docker-compose --version
```
Finally start docker-compose with the following command navigate to the web page: "http://localhost:4444"
```Powershell
sudo docker-compose up
```


## Description
The project contains 4 python modules:

* conftest.py
* locators.py
* page.py
* test_LoggingPage.py

### conftest.py
The *conftest.py* file is used as a means of providing fixtures for an entire directory. Fixtures defined in a conftest.py are used by any test in the package without needing to import them. 
* **SetUp**: it is a fixture used to correctly setup and teardown all the tests. During the setup the *webdriver.Firefox()* is instantiated to open the browser. From webdriver_manager, GeckoDriver is imported and using *service=Service(GeckoDriverManager().install())*, the matching geckodriver for firefox will be downloaded and installed in case it does not exist yet. In the teardown, the browser is closed.
* **logging_url**: it returns the logging URL to be used by any test
### locators.py
The *locators.py* contains all the locators of the web elements that can be found in the login page within the *LoggingPageLocators* class
### page.py
The *page.py* contains within the *Page* class all the methods to be used during the tests. For instance, the method to click on a web element, to verify that an element is clickable, to assert whether the correct url is reached

### test_LoggingPage.py
the *test_LoggingPage.py* contains all the tests defined to validate most of the webelements of loging web page. 
* **test_username**: this test searches for the webelement username, it verifies that the input field should contain an e-mail address.
* **test_password**: this test searches for the webelement password, it verifies that the input field should contain a password.
* **test_wrong_credentials**: this test verifies the behavior of the login page when the wrong credentials are typed in. It uses two parameters (username and password) and it fills these input fields with the method *send_text*. Then it checks whether the *Log in* button is clickable and it clicks on it. An assert condition has been added to verify whether the username chosen contains a '@' (email type). If so, the text *"Credentials are not valid"* are searched. Otherwise it triggers an Assertion Error.
* **test_dropdown_list**: this test checks that the dropdown list is clickable and then it clicks on it. After that, it hovers to all the elements contained in the list and click on each of them. Once it clicks on them, the browser will redirect to new URLs. An assert will check whether the correct webpages are reached. The locators of the webelements of the dropdown list and the corresponding URLs are defined as parameters of the test using *pytest.mark.parametrize*
* **test_forgot_password**: this test checks whether the *"Forgot your password?"* link button is clickable and it redirects to the correct URL
* **test_other_logging_links**: this test checks whether all the other logging methods buttons (Google, SAML and OpenID) are clickable and whether they have the corresponding correct *"Alt"* attribute
* **test_sign_up_free_trial**: this test checks whether the *"Sign up for a free trial!"* link is clickable and once clicked on it, whether it ridirects to the correct webpage asserting on its title.
## Usage
To run the tests, the pytest command has to be issued:
```Powershell
pytest -v test_loggingPage.py -s
```
The output result should be as the following:
```Powershell
================================================================================ test session starts =================================================================================
platform win32 -- Python 3.7.9, pytest-7.1.1, pluggy-1.0.0 -- "your_path"
cachedir: .pytest_cache
rootdir: "your_path"
collected 10 items

 
====== WebDriver manager ======    
Current firefox version is 99.0
Get LATEST geckodriver version for 99.0 firefox
Driver ["your_path"] found in cache
test_LoggingPage.py::test_username PASSED
test_LoggingPage.py::test_password PASSED
test_LoggingPage.py::test_wrong_credentials[user@name.com-password] PASSED
test_LoggingPage.py::test_dropdown_menu[dropdown_locator0-https://eu1.app.sysdig.com/#/login] PASSED
test_LoggingPage.py::test_dropdown_menu[dropdown_locator1-https://app.sysdigcloud.com/#/login] PASSED
test_LoggingPage.py::test_dropdown_menu[dropdown_locator2-https://us2.app.sysdig.com/#/login] PASSED
test_LoggingPage.py::test_dropdown_menu[dropdown_locator3-https://app.au1.sysdig.com/#/login] PASSED
test_LoggingPage.py::test_forgot_password PASSED
test_LoggingPage.py::test_other_logging_links PASSED
test_LoggingPage.py::test_sign_up_free_trial PASSED

================================================================================ 10 passed in 52.94s =================================================================================  
```


## Project improvements
Possible improvements for this project:
* Create a package python project
* Use Selenium With Python in a Docker Container
* Create correct login credentials and verify that the Log in redirects to the expected URL
* Extend the page.py script using a BasePage class that includs all the common methods. Add new classes for any other page that will inherit the methods from the BasePage.

