import pytest
from locators import LoginPageLocators
from page import Page
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *


def test_username(SetUp, logging_url):
    SetUp.get(logging_url)
    user_name_input = WebDriverWait(SetUp, 10).until(EC.visibility_of_element_located(LoginPageLocators.USERNAME))
    assert user_name_input.tag_name == 'input'
    assert user_name_input.get_attribute("type") == 'email'
    assert user_name_input.get_attribute("name") == 'username'


def test_password(SetUp, logging_url):
    SetUp.get(logging_url)
    password_input = WebDriverWait(SetUp, 10).until(EC.visibility_of_element_located(LoginPageLocators.PASSWORD))
    assert password_input.tag_name == 'input'
    assert password_input.get_attribute("type") == 'password'
    assert password_input.get_attribute("name") == 'password'


@pytest.mark.parametrize('username, password', [("user@name.com","password")])
def test_wrong_credentials(SetUp, logging_url, username, password):
    SetUp.get(logging_url)
    Page(SetUp).send_text(LoginPageLocators.USERNAME, username)
    Page(SetUp).send_text(LoginPageLocators.PASSWORD, password)
    Page(SetUp).is_clickable(LoginPageLocators.LOGIN_BUTTON)
    Page(SetUp).click(LoginPageLocators.LOGIN_BUTTON)
    try:
        Page(SetUp).assert_elem_text(LoginPageLocators.LOGIN_ERROR, "Credentials are not valid")
    except TimeoutException:
        assert False, "Username is email type, please include an '@' \n"

@pytest.mark.parametrize("dropdown_locator, new_logging_url",
[(LoginPageLocators.DROPDOWN_EU, "https://eu1.app.sysdig.com/#/login"),
(LoginPageLocators.DROPDOWN_US_EST, "https://app.sysdigcloud.com/#/login"),
(LoginPageLocators.DROPDOWN_US_WEST, "https://us2.app.sysdig.com/#/login"),
(LoginPageLocators.DROPDOWN_AP, "https://app.au1.sysdig.com/#/login")])
def test_dropdown_list(SetUp, logging_url, dropdown_locator, new_logging_url):
    SetUp.get(logging_url)
    Page(SetUp).is_clickable(LoginPageLocators.DROPDOWN_TAB)
    Page(SetUp).click(LoginPageLocators.DROPDOWN_TAB)
    Page(SetUp).is_clickable(dropdown_locator)
    Page(SetUp).hover_to(dropdown_locator).click().perform()
    Page(SetUp).assert_url(new_logging_url)


def test_forgot_password(SetUp, logging_url):
    SetUp.get(logging_url)
    Page(SetUp).is_clickable(LoginPageLocators.FORGOT_PASS_LINK)
    Page(SetUp).click(LoginPageLocators.FORGOT_PASS_LINK)
    Page(SetUp).assert_url("https://app.sysdigcloud.com/#/forgotPassword")


def test_other_logging_links(SetUp, logging_url):
    SetUp.get(logging_url)
    Page(SetUp).is_clickable(LoginPageLocators.GOOGLE_LINK)
    Page(SetUp).is_clickable(LoginPageLocators.SAML_LINK)
    Page(SetUp).is_clickable(LoginPageLocators.OPENID_LINK)
    google_link = WebDriverWait(SetUp, 10).until(EC.visibility_of_element_located(LoginPageLocators.GOOGLE_LINK))
    saml_link = WebDriverWait(SetUp, 10).until(EC.visibility_of_element_located(LoginPageLocators.SAML_LINK))
    openid_link = WebDriverWait(SetUp, 10).until(EC.visibility_of_element_located(LoginPageLocators.OPENID_LINK))
    assert google_link.get_attribute("alt") == 'Google'
    assert saml_link.get_attribute("alt") == 'SAML'
    assert openid_link.get_attribute("alt") == 'OpenID'


def test_sign_up_free_trial(SetUp, logging_url):
    SetUp.get(logging_url)
    Page(SetUp).is_clickable(LoginPageLocators.SIGN_UP)
    Page(SetUp).click(LoginPageLocators.SIGN_UP)
    assert "start free trial | sysdig" in SetUp.title.lower()



